import qualified Data.Map as M
import Data.List
data AExp = Var Char | Value Int | Add AExp AExp | Mul AExp AExp | Sub AExp AExp | Inc AExp deriving (Show)
data BExp = BVar Char| Eq AExp AExp | Lt AExp AExp | Gt AExp AExp | ELt AExp AExp| EGt AExp AExp | And BExp BExp | Or BExp BExp |Boolean Bool deriving (Show) 
data AssignExp = A AExp Char |B BExp Char  deriving (Show)
--data AssignExp = A Char Int  | B Char Bool 
data BoolInt = MyBool Bool | MyInt Int deriving (Show)
data Statement = AE AssignExp | While BExp [Statement] deriving (Show) 

type AEnv   = [(Char,BoolInt)] 
--type BEnv   = M.Map Char Bool

-- To run Use -> toString $ evalStmt (AE (A (Value 3) 'P')) aenv
-- toString $ evalStmt (While (Eq (Var 'C') (Value 3))  [AE (A (Inc (Var 'C')) 'C')]) aenv

aenv = [('A',(MyInt 1)),('B',(MyInt 2)),('C',(MyInt 3)),('D',(MyBool False))]::AEnv
--benv = M.fromList[('X',True)]

toString :: AEnv -> String
toString [] = ""
toString ((ch,(MyInt x)):env) = "("++(show (ch)) ++ "," ++ (show x) ++")"++ (toString env) 

toString ((ch,(MyBool x)):env) = "("++(show (ch)) ++ "," ++ (show x) ++")"++ (toString env) 

alookup :: Char -> AEnv -> Maybe BoolInt
alookup e [] = Nothing
alookup e ((a,b):xs) | e==a = Just b
                                   | otherwise = alookup e xs
minsert :: Char -> BoolInt -> AEnv -> AEnv
minsert ch v []= [(ch,v)]
minsert ch v ((a,b):xs) | ch==a = ((a,v):xs)
                                   | otherwise = ((a,b):(minsert ch v xs))
--minsert ch v env= ((ch,v):env)


extractInt Nothing = Nothing
extractInt (Just (MyInt x)) = (Just x)
extractInt (Just (MyBool x)) = (Nothing)

evalAExp :: AExp -> Maybe Int
evalAExp (Value x) = (Just x)
evalAExp (Var x) = extractInt (alookup x aenv)
evalAExp (Add a1 a2) =do 
  v2<-evalAExp a2;v1<-evalAExp a1
  return (v1+v2)
evalAExp (Sub a1 a2) =do 
  v2<-evalAExp a2;v1<-evalAExp a1
  return (v1-v2)  
evalAExp (Mul a1 a2) = do
  v1 <-evalAExp a1;v2 <- evalAExp a2
  return (v1*v2)
evalAExp (Inc a) =do 
  v<-evalAExp a
  return (v+1)  
  
evalBExp :: BExp -> Bool
evalBExp (Boolean True) = True 
evalBExp (Boolean False) = False 
evalBExp (BVar x) = 
  case (alookup x aenv) of
    Just (MyBool a) -> a
    _ -> False
evalBExp (Eq a1 a2) = (evalAExp (a1)) == (evalAExp (a2)) 
evalBExp (Lt a1 a2) = (evalAExp (a1)) < (evalAExp (a2))   
evalBExp (Gt a1 a2) = (evalAExp (a1)) > (evalAExp (a2))   
evalBExp (ELt a1 a2) = (evalAExp (a1)) <= (evalAExp (a2))   
evalBExp (EGt a1 a2) = (evalAExp (a1)) >= (evalAExp (a2))   
evalBExp (And b1 b2) = (evalBExp b1) && (evalBExp b2)   
evalBExp (Or b1 b2) = (evalBExp b1) || (evalBExp b2)

evalAssign :: AssignExp -> AEnv -> AEnv
evalAssign (A a ch) env=  
  case (evalAExp a) of
    Just x -> minsert ch (MyInt x) (env)
    Nothing -> env
    
evalAssign (B b ch) env =  minsert ch (MyBool(evalBExp b)) (env)   

-- To run Use -> toString $ evalStmt (AE (A (Value 3) 'P')) aenv
--Or
-- toString $ evalStmt (While (Eq (Var 'C') (Value 3))  [AE (A (Inc (Var 'C')) 'C')]) aenv
evalStmt :: Statement -> AEnv -> AEnv 
evalStmt (AE a) env = (evalAssign a env)
  
evalStmt (While b []) env = env                      
evalStmt (While b (s:st)) env=  
  case (evalBExp b) of
    True -> let m = (evalStmt s env) in evalStmt (While b st) m
    _ -> env
    
--toString $ evalStmt (While (Eq (Var 'C') (Value 3))  [AE (A (Inc (Var 'C')) 'C')]) aenv




