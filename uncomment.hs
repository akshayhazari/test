import Data.List
skip :: String -> String
skip "" = ""
skip (s:"") = ""
skip (s:t:str)  
   | s == '-' && t=='}' = str  
   | otherwise = skip (t:str)
                 
skipn :: String -> String                 
skipn "" = ""
skipn (s:str) 
  | s == '\n' = str
  | otherwise = skipn str
 
mySplit :: Maybe Int -> String -> (String,String)
mySplit Nothing str = ("",str)
mySplit (Just x) str = splitAt (x+1) str  

continuec :: String -> (String,String)                 
continuec "" = ("","")
continuec str  = mySplit (elemIndex '\"' str) str

uncomment :: String -> String
uncomment "" = ""
uncomment (s:"") = s:""
uncomment (s:t:str) 
  | s == '\"' = let (a,b) = continuec (t:str) in ((s:a) ++ uncomment b) 
  | s == '{' && t=='-' =  uncomment (skip str) 
  | s == '-' && t == '-' = uncomment (skipn str)
  | otherwise = s:(uncomment (t:str))

uncommentFile :: FilePath -> IO ()
uncommentFile filename = do                
  content <- readFile filename
  print $  uncomment content
